//
//  ConstantStringCheck.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 28.02.22.
//

import Foundation

struct ConstantStringCheck {
    static let currentStatusOn = "On"
    static let currentStatusOff = "Off"
    static let currentStatusTempOff = "TempOff"
    static let serviceTypeCashWithdrawal = "CashWithdrawal"
    static let serviceTypePinChange = "PINChange"
    static let serviceTypePINUnblock = "PINUnblock"
    static let serviceTypePINActivation = "PINActivation"
    static let serviceTypeBalance = "Balance"
    static let serviceTypeMiniStatement = "MiniStatement"
    static let serviceTypeBillPayments = "BillPayments"
    static let serviceTypeMobileBankingRegistration = "MobileBankingRegistration"
    static let serviceTypeCurrencyExhange = "CurrencyExhange"
    static let serviceTypeCashIn = "CashIn"
    static let serviceTypeOther = "Other"
}
