//
//  ATM+Ex.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 27.02.22.
//

import Foundation

extension ATM {
    static func createWorkingHoursList(days: [Day]?) -> String {
        if let days = days {
            let daysString = ["пн", "вт", "ср", "чт", "пт", "сб", "вс"]
            var workingHoursList: [String] = []
            for day in days {
                var tmpStr = ""
                tmpStr += "\(day.openingTime)-\(day.closingTime)"
                if day.pause.breakFromTime != day.pause.breakToTime {
                    tmpStr += " Перерыв \(day.pause.breakFromTime)-\(day.pause.breakToTime)"
                }
                workingHoursList.append(tmpStr)
            }
            var index = 1
            var tmpDay = "\(daysString[0])"
            var tmpWorkingHours = "\(daysString[0])"
            while index < workingHoursList.count {
                if workingHoursList[index - 1] != workingHoursList[index] {
                    if tmpDay != daysString[index - 1] {
                        tmpWorkingHours +=
                        "-\(daysString[index - 1]) \(workingHoursList[index - 1]) \(daysString[index])"
                        tmpDay = daysString[index]
                    } else {
                        tmpWorkingHours += " \(workingHoursList[index - 1]) \(daysString[index])"
                    }
                }
                index += 1
            }
            if tmpDay != daysString[index - 1] {
                tmpWorkingHours += "-\(daysString[index - 1]) \(workingHoursList[index - 1])"
            } else {
                tmpWorkingHours += " \(workingHoursList[index - 1])"
            }
            return tmpWorkingHours
        }
        return ""
    }

    static func isCachIn(services: [Services]) -> Bool {
        for service in services where service.serviceType == "CashIn" {
            return true
        }
        return false
    }

    static func getCards(cards: [String]?) -> String {
        if let cards = cards {
            var allCards = ""
            for card in cards {
                allCards += "\(card) "
            }
            return allCards
        }
        return ""
    }

    static func getCurrentStatus(currentStatus: String?) -> String {
        if let currentStatus = currentStatus {
            switch currentStatus {
            case ConstantStringCheck.currentStatusOn:
                return ConstantString.currentStatusOn
            case ConstantStringCheck.currentStatusOff:
                return ConstantString.currentStatusOff
            case ConstantStringCheck.currentStatusTempOff:
                return ConstantString.currentStatusTempOff
            default:
                break
            }
        }
        return ""
    }

    static func getCordinate(coordinates: GeographicCoordinates?) -> String {
        if let coordinates = coordinates {
            return "\(coordinates.latitude), \(coordinates.longitude)"
        }
        return ""
    }

    static func getAddress(address: Address?) -> String {
        if let address = address {
            var fullAdress = ""
            if address.streetName != "" {
                fullAdress += "\(address.streetName), "
            }
            if address.buildingNumber != "" {
                fullAdress += "\(address.buildingNumber), "
            }
            if address.townName != "" {
                fullAdress += "\(address.townName), "
            }
            if address.countrySubDivision != "" {
                if address.countrySubDivision != "Минск" {
                    fullAdress += "\(address.countrySubDivision) обл. "
                }
            }
            return fullAdress
        }
        return ""
    }

    static func checkService(_ serviceType: String, in services: [Services]?) -> String {
        if let services = services {
            for service in services where service.serviceType == serviceType {
                if service.description == "" {
                    return ConstantString.yes
                } else {
                    return service.description
                }
            }
        }
        return ConstantString.not
    }
}
