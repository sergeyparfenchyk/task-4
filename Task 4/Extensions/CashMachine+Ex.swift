//
//  CashMachine+Ex.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 24.02.22.
//

import Foundation

extension CashMachine {
    static var urlRequest: URLRequest {
        let url = URL(string: "https://belarusbank.by/open-banking/v1.0/atms")!
        let request = URLRequest(url: url)
        return request
    }
}
