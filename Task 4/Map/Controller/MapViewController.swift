//
//  MapViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 23.02.22.
//

import UIKit
import MapKit

// MARK: - Constants

private extension ConstantString {
    static let pointId = "PointId"
}

class MapViewController: UIViewController {

    // MARK: - Public Properties

    var atms: [ATM] = []

    // MARK: - Private Properties

    private let locationManager = CLLocationManager()
    private lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.delegate = self
        return mapView
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mapView)
        mapView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            mapView.showsUserLocation = true
        }

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setAtms),
                                               name: ConstantNotificationName.didSetAtms,
                                               object: nil)
    }

    // MARK: Private Methods

    @objc private func setAtms(notification: Notification) {
        if let atms = notification.object as? [ATM] {
            self.atms = atms
        }
        addPointInMap()
    }

    @objc private func hideCallout() {
        mapView.deselectAnnotation(nil, animated: true)
    }

    private func addPointInMap() {
        for (index, atm) in atms.enumerated() {
            if let latitude = Double(atm.address.geolocation.geographicCoordinates.latitude),
               let longitude = Double(atm.address.geolocation.geographicCoordinates.longitude) {
                let pin = CashMashinPointAnnotation()
                pin.indexInArrayATM = index
                pin.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                mapView.addAnnotation(pin)
            }
        }
        DispatchQueue.main.async {
            self.mapView.showAnnotations(self.mapView.annotations, animated: true)
            if let location = self.mapView.userLocation.location {
                self.focusUser(location: location)
            }
        }
    }

    private func focusUser(location: CLLocation) {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                                            longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center,
                                         span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        mapView.setRegion(region, animated: true)
    }

    @objc private func openAllInformationATM(button: UIButton) {
        let allInformationATMViewController = AllInformationATMViewController()
        allInformationATMViewController.atm = atms[button.tag]
        present(allInformationATMViewController, animated: true, completion: nil)
    }
}

// MARK: MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let cashMashinPointAnnotation = annotation as? CashMashinPointAnnotation else { return nil}
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: ConstantString.pointId)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = cashMashinPointAnnotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: ConstantString.pointId)
        }
        let indexATM = cashMashinPointAnnotation.indexInArrayATM
        let atm = atms[indexATM]
        view.canShowCallout = true
        let calloutView = ATMInfoView()
        calloutView.placeLable.text = atm.address.addressLine
        calloutView.workingHoursLable.text = ATM.createWorkingHoursList(days: atm.availability.standardAvailability.day)
        calloutView.currencyLable.text = "\(ConstantString.currency): \(atm.currency)"
        let isCashIn = "\(ATM.isCachIn(services: atm.services) ? ConstantString.yes : ConstantString.not)"
        calloutView.cashInLable.text = "\(ConstantString.serviceTypeCashIn): \(isCashIn)"
        calloutView.moreDetailedButton.tag = indexATM
        calloutView.moreDetailedButton.addTarget(self, action: #selector(openAllInformationATM), for: .touchUpInside)
        calloutView.closeButton.addTarget(self, action: #selector(hideCallout), for: .touchUpInside)
        view.detailCalloutAccessoryView = calloutView
        return view
    }
}

// MARK: CLLocationManagerDelegate

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0] as CLLocation
        focusUser(location: userLocation)
    }
}
