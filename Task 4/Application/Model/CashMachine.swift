//
//  CashMachine.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 24.02.22.
//

import Foundation

struct CashMachine: Requestable {
    var data: Data
    enum CodingKeys: String, CodingKey {
        case data = "Data"
    }
}

struct Data: Codable {
    var atm: [ATM]
    enum CodingKeys: String, CodingKey {
        case atm = "ATM"
    }
}
struct ATM: Codable {
    var atmId: String
    var type: String
    var baseCurrency: String
    var currency: String
    var cards: [String]
    var currentStatus: String
    var address: Address
    var services: [Services]
    var availability: Availability
    var contactDetails: ContactDetails
    enum CodingKeys: String, CodingKey {
        case atmId
        case type
        case baseCurrency
        case currency
        case cards
        case currentStatus
        case address = "Address"
        case services = "Services"
        case availability = "Availability"
        case contactDetails = "ContactDetails"
    }
}
struct Address: Codable {
    var streetName: String
    var buildingNumber: String
    var townName: String
    var countrySubDivision: String
    var country: String
    var addressLine: String
    var description: String
    var geolocation: Geolocation
    enum CodingKeys: String, CodingKey {
        case streetName
        case buildingNumber
        case townName
        case countrySubDivision
        case country
        case addressLine
        case description
        case geolocation = "Geolocation"
    }
}
struct Geolocation: Codable {
    var geographicCoordinates: GeographicCoordinates
    enum CodingKeys: String, CodingKey {
        case geographicCoordinates = "GeographicCoordinates"
    }
}
struct GeographicCoordinates: Codable {
    var latitude: String
    var longitude: String
}
struct Services: Codable {
    var serviceType: String
    var description: String
}
struct Availability: Codable {
    var access24Hours: Bool
    var isRestricted: Bool
    var sameAsOrganization: Bool
    var standardAvailability: StandardAvailability
    enum CodingKeys: String, CodingKey {
        case access24Hours
        case isRestricted
        case sameAsOrganization
        case standardAvailability = "StandardAvailability"
    }
}
struct StandardAvailability: Codable {
    var day: [Day]
    enum CodingKeys: String, CodingKey {
        case day = "Day"
    }
}
struct Day: Codable {
    var dayCode: String
    var openingTime: String
    var closingTime: String
    var pause: Break
    enum CodingKeys: String, CodingKey {
        case dayCode
        case openingTime
        case closingTime
        case pause = "Break"
    }
}
struct Break: Codable {
    var breakFromTime: String
    var breakToTime: String
}
struct ContactDetails: Codable {
    var phoneNumber: String
}

protocol Requestable: Decodable {
    static var urlRequest: URLRequest { get }
}
