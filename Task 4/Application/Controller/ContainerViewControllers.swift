//
//  ContainerViewControllers.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 22.02.22.
//

import UIKit
import SnapKit
import Network

// MARK: Constants

private extension ConstantString {
    static let atmS = "Банкоматы"
    static let map = "Карта"
    static let list = "Список"
    static let titleAlert = "Интернет-соединение отсутствует"
    static let descriptionAlert = "Приложение не работает без доступа к интернету."
    static let buttonAllert = "Хорошо"
}

private struct ConstantSize {
    static let heighMapOrListSegmentedControl = 30.0
}

class ContainerViewControllers: UIViewController {

    // MARK: Public Properties

    var atms: [ATM] = [] {
        didSet {
            NotificationCenter.default.post(name: ConstantNotificationName.didSetAtms,
                                            object: atms,
                                            userInfo: nil)
        }
    }

    // MARK: Private Properties

    private let monitor = NWPathMonitor()
    private let queue = DispatchQueue(label: "InternetConnectionMonitor")
    private lazy var updateRightButton = UIBarButtonItem(barButtonSystemItem: .refresh,
                                                         target: self,
                                                         action: #selector(updateData))
    private lazy var mapViewController = MapViewController()
    private lazy var listViewController = ListViewController()
    private lazy var mapAction: UIAction = {
        let mapAction = UIAction(title: ConstantString.map) {(_) in
            self.remove(child: self.listViewController)
            self.add(child: self.mapViewController)
        }
        return mapAction
    }()

    private lazy var listAction: UIAction = {
        let listAction = UIAction(title: ConstantString.list) {(_) in
            self.remove(child: self.mapViewController)
            self.add(child: self.listViewController)
        }
        return listAction
    }()

    private lazy var mapOrListSegmentedControl: UISegmentedControl = {
        let mapOrListSegmentedControl = UISegmentedControl(frame: .zero,
                                                           actions: [mapAction, listAction])
        mapOrListSegmentedControl.selectedSegmentIndex = 0
        view.addSubview(mapOrListSegmentedControl)
        mapOrListSegmentedControl.snp.makeConstraints { make in
            make.height.equalTo(ConstantSize.heighMapOrListSegmentedControl)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
        }
        return mapOrListSegmentedControl
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        updateRightButton.isEnabled = false
        view.backgroundColor = ConstantColor.background
        title = ConstantString.atmS
        checkInternet()
        add(child: mapViewController)
        navigationItem.rightBarButtonItem = updateRightButton
    }

    // MARK: Private Methods

    @objc private func updateData() {
        getDataATM()
        updateRightButton.isEnabled = false
        monitor.start(queue: queue)
    }

    private func getDataATM() {
        URLSession.performRequest(on: CashMachine.self) { (result) in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let object):
                self.atms = object.data.atm.sorted(by: { atmFirts, atmSecond in
                    if atmFirts.address.townName < atmSecond.address.townName {
                        return true
                    } else if atmFirts.address.townName > atmSecond.address.townName {
                        return false
                    } else if atmFirts.atmId < atmSecond.atmId {
                        return true
                    }
                    return false
                })
            }
            DispatchQueue.main.async {
                self.updateRightButton.isEnabled = true
            }
        }
    }

    private func checkInternet() {
        monitor.pathUpdateHandler = { pathUpdateHandler in
            if pathUpdateHandler.status == .satisfied {
                self.getDataATM()
            } else {
                let alert = UIAlertController(title: ConstantString.titleAlert,
                                              message: ConstantString.descriptionAlert,
                                              preferredStyle: .alert)
                let action = UIAlertAction(title: ConstantString.buttonAllert,
                                           style: .default,
                                           handler: nil)
                alert.addAction(action)
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                    self.updateRightButton.isEnabled = true
                }
            }
        }
        monitor.start(queue: queue)
    }

    private func add(child viewController: UIViewController) {
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.view.snp.makeConstraints { make in
            make.top.equalTo(mapOrListSegmentedControl.snp.bottom)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
            make.bottom.equalTo(view.snp.bottom)
        }
        viewController.didMove(toParent: self)
    }

    private func remove(child viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
}
