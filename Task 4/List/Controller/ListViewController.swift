//
//  ListViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 23.02.22.
//

import UIKit

// MARK: Constants

private extension ConstantString {
    static let cellId = "CellId"
    static let sectionId = "SectionId"
}

private struct ConstantSize {
    static let headerSection = 20.0
    static let minimumLineSpacingForSectionAt = 10.0
    static let minimumInteritemSpacingForSectionAt = 10.0
    static let numberRows = 3
    static let heightCell = 150.0
    static let offsetCell = 10.0
}

class ListViewController: UIViewController {

    // MARK: Public Properties

    var atms: [ATM] = []

    // MARK: Private Properties

    private var cellsInSection = [0]
    private var titlesSection: [String] = []
    private lazy var atmCollectionView: UICollectionView = {
        atmCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        atmCollectionView.register(AtmCollectionViewCell.self, forCellWithReuseIdentifier: ConstantString.cellId)
        atmCollectionView.register(NameCityCollectionReusableView.self,
                                   forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                   withReuseIdentifier: ConstantString.sectionId)
        view.addSubview(atmCollectionView)
        setContraints()
        return atmCollectionView
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        atmCollectionView.delegate = self
        atmCollectionView.dataSource = self

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setAtms),
                                               name: ConstantNotificationName.didSetAtms,
                                               object: nil)
        if let parentViewController = self.parent as? ContainerViewControllers {
            atms = parentViewController.atms
            if atms.count > 0 {
                countRowAndSection()
                atmCollectionView.reloadData()
            }
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self,
                                                  name: ConstantNotificationName.didSetAtms,
                                                  object: nil)
        self.atms = []
        cellsInSection = [0]
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        atmCollectionView.reloadData()
    }

    // MARK: Private Method

    @objc private func setAtms(notification: Notification) {
        if let atms = notification.object as? [ATM] {
            self.atms = atms
        }
        reloadDataAtmCollectionView()
    }

    private func reloadDataAtmCollectionView() {
        countRowAndSection()
        DispatchQueue.main.async {
            self.atmCollectionView.reloadData()
        }
    }

    private func setContraints() {
        atmCollectionView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview()
        }
    }

    private func countRowAndSection() {
        var section = 0
        var index = 1
        while index < atms.count {
            if atms[index - 1].address.townName != atms[index].address.townName {
                cellsInSection[section] += 1
                cellsInSection.append(0)
                titlesSection.append(atms[index - 1].address.townName)
                section += 1
            } else {
                cellsInSection[section] += 1
            }
            index += 1
        }
        cellsInSection[section] += 1
    }
}

// MARK: - UICollectionViewDataSource

extension ListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return cellsInSection[section]
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return cellsInSection.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        if let headerSection = collectionView.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: ConstantString.sectionId,
            for: indexPath) as? NameCityCollectionReusableView {

            if  titlesSection.count > indexPath.section {
                headerSection.nameCityLable.text = titlesSection[indexPath.section]
            }
            return headerSection
        }
        let headerSection = NameCityCollectionReusableView()
        return headerSection
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {

        return CGSize(width: collectionView.bounds.size.width, height: ConstantSize.headerSection)
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConstantString.cellId,
                                                         for: indexPath) as? AtmCollectionViewCell {

            let item = indexPath.item + indexPath.section
            cell.placeDescriptionLable.text = atms[item].address.addressLine
            if atms[item].availability.access24Hours {
                cell.workingHoursDescriptionLable.text = ConstantString.access24Hours
            } else {
                let days = atms[item].availability.standardAvailability.day
                let workingHours = ATM.createWorkingHoursList(days: days)
                cell.workingHoursDescriptionLable.text = workingHours
            }
            cell.currencyDescriptionLable.text = atms[item].currency
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConstantString.cellId, for: indexPath)
        return cell
    }
}

// MARK: UICollectionViewDelegate

extension ListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let allInformationATMViewController = AllInformationATMViewController()
        let item = indexPath.item + indexPath.section
        allInformationATMViewController.atm = atms[item]
        present(allInformationATMViewController, animated: true, completion: nil)
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension ListViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (view.frame.width - ConstantSize.offsetCell *
                              Double((ConstantSize.numberRows - 1))) /
                      Double(ConstantSize.numberRows),
                      height: ConstantSize.heightCell)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return ConstantSize.minimumLineSpacingForSectionAt
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return ConstantSize.minimumInteritemSpacingForSectionAt
     }
}
