//
//  DescriptionLable.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 26.02.22.
//

import UIKit

// MARK: Constatnts

private struct ConstantSize {
    static let fontDescription = 12.0
}

class DescriptionLable: UILabel {

    // MARK: Initialization

    init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        font = UIFont.systemFont(ofSize: ConstantSize.fontDescription)
        adjustsFontSizeToFitWidth = true
    }
}
