//
//  AllInformationATMViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 27.02.22.
//

import UIKit
import MapKit

// MARK: Constants

private extension ConstantString {
    static let cellId = "cellId"
    static let titleRouteButton = "Построить маршрут"
}

private extension ConstantColor {
    static let routeButton = UIColor.systemGreen
}

private struct ConstantSize {
    static let numberOfRows = 25
    static let heighRow = 70.0
    static let topOffestTableView = 20.0
    static let heightRouteButton = 70.0
}

class AllInformationATMViewController: UIViewController {

    // MARK: Public Properties

    var atm: ATM?

    // MARK: Private Properties
    private lazy var infoTableView: UITableView = {
        let infoTableView = UITableView()
        infoTableView.delegate = self
        infoTableView.dataSource = self
        infoTableView.register(TitleAndDescriptionTableViewCell.self,
                           forCellReuseIdentifier: ConstantString.cellId)
        infoTableView.separatorColor = .clear
        return infoTableView
    }()

    private lazy var routeButton: UIButton = {
        let routeButton = UIButton()
        routeButton.backgroundColor = ConstantColor.routeButton
        routeButton.setTitle(ConstantString.titleRouteButton, for: .normal)
        routeButton.addTarget(self, action: #selector(openMapForRoute), for: .touchUpInside)
        return routeButton
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.background
        view.addSubview(infoTableView)
        view.addSubview(routeButton)
        infoTableView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(ConstantSize.topOffestTableView)
            make.bottom.equalTo(routeButton.snp.top)
            make.leading.trailing.equalToSuperview()
        }
        routeButton.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(ConstantSize.heightRouteButton)
            make.bottom.equalToSuperview()
        }
    }

    // MARK: Private method

    @objc private func openMapForRoute() {
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        if let latitude = atm?.address.geolocation.geographicCoordinates.latitude,
           let longitude = atm?.address.geolocation.geographicCoordinates.longitude {
            if let latitude = Double(latitude), let longitude = Double(longitude) {
                let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                let placemark = MKPlacemark(coordinate: locationCoordinate)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.openInMaps(launchOptions: launchOptions)
            }
        }
    }
}

// MARK: - UITableViewDelegate

extension AllInformationATMViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ConstantSize.heighRow
    }
}

// MARK: - UITableViewDataSource

extension AllInformationATMViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ConstantSize.numberOfRows
    }
    // swiftlint:disable all
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellTitleDescription: TitleAndDescriptionTableViewCell
        if let cell = tableView.dequeueReusableCell(
            withIdentifier: ConstantString.cellId) as? TitleAndDescriptionTableViewCell {
            cellTitleDescription = cell
        } else {
            cellTitleDescription = TitleAndDescriptionTableViewCell()
        }
        cellTitleDescription.selectionStyle = .none
        switch indexPath.row {
        case 0:
            cellTitleDescription.titleLabel.text = ConstantString.atmId
            cellTitleDescription.descriptionLable.text = atm?.atmId
        case 1:
            cellTitleDescription.titleLabel.text = ConstantString.type
            cellTitleDescription.descriptionLable.text = atm?.type
        case 2:
            cellTitleDescription.titleLabel.text = ConstantString.baseCurrency
            cellTitleDescription.descriptionLable.text = atm?.baseCurrency
        case 3:
            cellTitleDescription.titleLabel.text = ConstantString.cards
            cellTitleDescription.descriptionLable.text = ATM.getCards(cards: atm?.cards)
        case 4:
            cellTitleDescription.titleLabel.text = ConstantString.currentStatus
            cellTitleDescription.descriptionLable.text = ATM.getCurrentStatus(currentStatus: atm?.currentStatus)
        case 5:
            cellTitleDescription.titleLabel.text = ConstantString.address
            cellTitleDescription.descriptionLable.text = ATM.getAddress(address: atm?.address)
        case 6:
            cellTitleDescription.titleLabel.text = ConstantString.addressLine
            cellTitleDescription.descriptionLable.text = atm?.address.addressLine
        case 7:
            cellTitleDescription.titleLabel.text = ConstantString.addresDescription
            cellTitleDescription.descriptionLable.text = atm?.address.description
        case 8:
            cellTitleDescription.titleLabel.text = ConstantString.сoordinates
            cellTitleDescription.descriptionLable.text =
            ATM.getCordinate(coordinates: atm?.address.geolocation.geographicCoordinates)
        case 9:
            cellTitleDescription.titleLabel.text = ConstantString.isRestricted
            let restricted = atm?.availability.isRestricted ?? false ? ConstantString.yes : ConstantString.not
            cellTitleDescription.descriptionLable.text = restricted
        case 10:
            cellTitleDescription.titleLabel.text = ConstantString.availability
            let day = ATM.createWorkingHoursList(days: atm?.availability.standardAvailability.day)
            cellTitleDescription.descriptionLable.text = day
        case 11:
            cellTitleDescription.titleLabel.text = ConstantString.sameAsOrganization
            let sameAsOrganization = atm?.availability.sameAsOrganization ??
            false ? ConstantString.yes : ConstantString.not
            cellTitleDescription.descriptionLable.text = sameAsOrganization
        case 12:
            cellTitleDescription.titleLabel.text = ConstantString.phoneNumber
            cellTitleDescription.descriptionLable.text = atm?.contactDetails.phoneNumber
        case 13:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypeCashWithdrawal
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypeCashWithdrawal,
                                                                          in: atm?.services)
        case 14:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypePinChange
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypePinChange,
                                                                          in: atm?.services)
        case 15:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypePINUnblock
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypePINUnblock,
                                                                          in: atm?.services)
        case 16:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypePINActivation
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypePINActivation,
                                                                          in: atm?.services)
        case 17:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypeBalance
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypeBalance,
                                                                          in: atm?.services)
        case 18:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypeMiniStatement
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypeMiniStatement,
                                                                          in: atm?.services)
        case 19:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypeBillPayments
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypeBillPayments,
                                                                          in: atm?.services)
        case 20:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypeMobileBankingRegistration
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypeMobileBankingRegistration,
                                                                          in: atm?.services)
        case 21:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypeCurrencyExhange
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypeCurrencyExhange,
                                                                          in: atm?.services)
        case 22:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypeCurrencyExhange
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypeCurrencyExhange,
                                                                          in: atm?.services)
        case 23:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypeCashIn
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypeCashIn,
                                                                          in: atm?.services)
        case 24:
            cellTitleDescription.titleLabel.text = ConstantString.serviceTypeOther
            cellTitleDescription.descriptionLable.text = ATM.checkService(ConstantStringCheck.serviceTypeOther,
                                                                          in: atm?.services)
        default:
            break
        }
        return cellTitleDescription
    }
}
