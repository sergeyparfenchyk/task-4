//
//  TitleAndDescriptionTableViewCell.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 27.02.22.
//

import UIKit

// MARK: Constants

private struct ConstantSize {
    static let numberOfLinesTitle = 2
    static let numberOfLinesDescription = 4
    static let leadingAndTralingOffset = 10.0
    static let sizeFontDescription = 15.0
}

class TitleAndDescriptionTableViewCell: UITableViewCell {

    // MARK: Private Properties

    private(set) lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.numberOfLines = ConstantSize.numberOfLinesTitle
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(ConstantSize.leadingAndTralingOffset)
            make.trailing.equalToSuperview().offset(-ConstantSize.leadingAndTralingOffset)
            make.top.equalToSuperview()
        }
        return titleLabel
    }()

    private(set) lazy var descriptionLable: UILabel = {
        let descriptionLable = UILabel()
        descriptionLable.numberOfLines = ConstantSize.numberOfLinesDescription
        contentView.addSubview(descriptionLable)
        descriptionLable.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(ConstantSize.leadingAndTralingOffset)
            make.trailing.equalToSuperview().offset(-ConstantSize.leadingAndTralingOffset)
            make.top.equalTo(titleLabel.snp.bottom)
            make.bottom.lessThanOrEqualToSuperview()
        }
        descriptionLable.textColor = .systemGray2
        descriptionLable.font = UIFont.boldSystemFont(ofSize: ConstantSize.sizeFontDescription)
        return descriptionLable
    }()
}
